package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;
import tb.antlr.symbolTable.GlobalSymbols;

public class MyTreeParser extends TreeParser {

	protected GlobalSymbols globalSymbols = new GlobalSymbols();
	
    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }

    protected int podziel(int dzielna, int dzielnik) throws RuntimeException {
    	if (dzielnik == 0) throw new RuntimeException("Dzielenie przez 0");
    	return dzielna / dzielnik;
    }

    protected int potega(int podstawa, int wykladnik) {
    	int wynik = 1;
    	if (wykladnik == 0) return wynik;
    	for(int i = 0; i < wykladnik; i++) {
    		wynik *= podstawa;
    	}
    	return wynik;
    }
    
    protected void zadeklaruj(String zmienna) {
    	globalSymbols.newSymbol(zmienna);
    }
    
    protected void podstaw(String zmienna, int liczba) {
    	globalSymbols.setSymbol(zmienna, liczba);
    }
    
    protected int getVar(String zmienna) {
    	return globalSymbols.getSymbol(zmienna);
    }
    
	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
}
