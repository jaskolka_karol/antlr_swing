tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
}

prog    : (expr | decl)* ;

// ZAD DODATKOWE 3 - VAR
decl    : ^(VAR i1=ID) {zadeklaruj($i1.text);};

expr returns [Integer out]
// ZAD PODSTAWOWE
	      : ^(PLUS  e1=expr e2=expr)    {$out = $e1.out + $e2.out;}
        | ^(MINUS e1=expr e2=expr)    {$out = $e1.out - $e2.out;}
        | ^(MUL   e1=expr e2=expr)    {$out = $e1.out * $e2.out;}
        | ^(DIV   e1=expr e2=expr)    {$out = podziel($e1.out, $e2.out);}
        | ^(PODST i1=ID   e2=expr)    {$out = $e2.out; podstaw($i1.text, $e2.out);}
        | INT                         {$out = getInt($INT.text);}
// ZAD DODATKOWE 2 - POTEGOWANIE
        | ^(POW   e1=expr e2=expr)    {$out = potega($e1.out, $e2.out);}
// ZAD DODATKOWE 1 - PRINT
        | ^(PRINT e=expr)             {$out = $e.out; drukuj($e.out.toString());}
        | ID                          {$out = getVar($ID.text);}
        ;
